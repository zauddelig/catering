from grangala.catering.models import CategoriaCatering
from .models import VociMenu
qs = CategoriaCatering.objects
def menu(request):
    menu = qs.filter(principale = True)
    sottomenu = qs.filter(principale = False)
    altre_voci = VociMenu.objects.all()
    return {'menu':menu, 'sottomenu': sottomenu, 'altre_voci':altre_voci}

