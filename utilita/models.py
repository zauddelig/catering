from django.db import models as m
from django.contrib.flatpages.models import FlatPage

class Pagina(FlatPage):
    class Meta:
        proxy = True
        pass
    pass

class VociMenu(m.Model):
    nome = m.CharField(max_length=30)
    pagina = m.ForeignKey(Pagina)