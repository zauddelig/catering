from grangala.catering.models import CategoriaCatering
from grangala.catering.forms import MessaggioForm
from .models import VociMenu
qs = CategoriaCatering.objects
def menu(request):
    menu = qs.filter(principale = True)
    sottomenu = qs.filter(principale = False).order_by('nome')
    altre_voci = VociMenu.objects.all()
    return {'menu':menu, 'sottomenu': sottomenu, 'altre_voci':altre_voci}

def contatto(request):
    form_messaggio = MessaggioForm()
    return {'form_messaggio':form_messaggio}
    pass