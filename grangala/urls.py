from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'grangala.views.home', name='home'),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^newsletter/', include('grangala.lista_email.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('grangala.catering.urls')),
    url(r'^info(?P<url>.*)$', 'django.contrib.flatpages.views.flatpage', name='flatpage'),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

from grangala.lista_email.views import RinunciaIscrizione, RinunciaRiuscita,RinunciaNonEffettuata,RinunciaNonRiuscita

urlpatterns += patterns('',
    url(r'^newsletter/rimuvi_sottoscrizione/(?P<user>\d+)', RinunciaIscrizione.as_view(), name="rinuncia_iscrizione"),
    url(r'^newsletter/rinuncia_riuscita',RinunciaRiuscita.as_view(), name="rinuncia_utente_lista_riuscita" ),
    url(r'^newsletter/rinuncia_non_riuscita', RinunciaNonRiuscita.as_view(), name="rinuncia_utente_non_riuscita"),
    url(r'^newsletter/rinuncia_non_effettuata',RinunciaNonEffettuata.as_view(), name="rinuncia_utente_non_effettuata"),
)

from django.conf import settings
if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'static/(?P<path>.*)$', 'serve'),
        url(r'public/(?P<path>.*)$', 'serve'),
        
    )