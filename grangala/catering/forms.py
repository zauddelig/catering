from .models import Messaggio
from django.forms.models import ModelForm

class MessaggioForm(ModelForm):
    class Meta:
        model = Messaggio
        pass
    pass