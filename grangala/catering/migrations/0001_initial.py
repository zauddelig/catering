# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Cliente'
        db.create_table(u'catering_cliente', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('cognome', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal(u'catering', ['Cliente'])

        # Adding model 'CategoriaCatering'
        db.create_table(u'catering_categoriacatering', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('principale', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'catering', ['CategoriaCatering'])

        # Adding model 'Catering'
        db.create_table(u'catering_catering', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('evidenza', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('testo', self.gf('django.db.models.fields.TextField')()),
            ('titolo', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=100)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catering.Cliente'], null=True, blank=True)),
            ('tipo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catering.CategoriaCatering'])),
        ))
        db.send_create_signal(u'catering', ['Catering'])

        # Adding model 'ImmagineCatering'
        db.create_table(u'catering_immaginecatering', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('immagine', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('catering', self.gf('django.db.models.fields.related.ForeignKey')(related_name='galleria', to=orm['catering.Catering'])),
        ))
        db.send_create_signal(u'catering', ['ImmagineCatering'])


    def backwards(self, orm):
        # Deleting model 'Cliente'
        db.delete_table(u'catering_cliente')

        # Deleting model 'CategoriaCatering'
        db.delete_table(u'catering_categoriacatering')

        # Deleting model 'Catering'
        db.delete_table(u'catering_catering')

        # Deleting model 'ImmagineCatering'
        db.delete_table(u'catering_immaginecatering')


    models = {
        u'catering.categoriacatering': {
            'Meta': {'object_name': 'CategoriaCatering'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'principale': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'catering.catering': {
            'Meta': {'object_name': 'Catering'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.Cliente']", 'null': 'True', 'blank': 'True'}),
            'evidenza': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100'}),
            'testo': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.CategoriaCatering']"}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        u'catering.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'cognome': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catering.immaginecatering': {
            'Meta': {'object_name': 'ImmagineCatering'},
            'catering': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'galleria'", 'to': u"orm['catering.Catering']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immagine': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['catering']