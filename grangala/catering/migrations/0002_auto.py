# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'CategoriaCatering', fields ['nome']
        db.create_index(u'catering_categoriacatering', ['nome'])


    def backwards(self, orm):
        # Removing index on 'CategoriaCatering', fields ['nome']
        db.delete_index(u'catering_categoriacatering', ['nome'])


    models = {
        u'catering.categoriacatering': {
            'Meta': {'object_name': 'CategoriaCatering'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'principale': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'catering.catering': {
            'Meta': {'object_name': 'Catering'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.Cliente']", 'null': 'True', 'blank': 'True'}),
            'evidenza': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100'}),
            'testo': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.CategoriaCatering']"}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        },
        u'catering.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'cognome': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catering.immaginecatering': {
            'Meta': {'object_name': 'ImmagineCatering'},
            'catering': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'galleria'", 'to': u"orm['catering.Catering']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immagine': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['catering']