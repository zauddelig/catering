# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Messaggio'
        db.create_table(u'catering_messaggio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('cognome', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('testo', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'catering', ['Messaggio'])


    def backwards(self, orm):
        # Deleting model 'Messaggio'
        db.delete_table(u'catering_messaggio')


    models = {
        u'catering.categoriacatering': {
            'Meta': {'object_name': 'CategoriaCatering'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'principale': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'catering.catering': {
            'Meta': {'object_name': 'Catering'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.Cliente']", 'null': 'True', 'blank': 'True'}),
            'evidenza': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '125'}),
            'testo': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.CategoriaCatering']"}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '101'})
        },
        u'catering.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'cognome': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catering.immaginecatering': {
            'Meta': {'object_name': 'ImmagineCatering'},
            'catering': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'galleria'", 'to': u"orm['catering.Catering']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immagine': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'})
        },
        u'catering.messaggio': {
            'Meta': {'object_name': 'Messaggio'},
            'cognome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'testo': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['catering']