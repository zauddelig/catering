# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Catering.cardinalita'
        db.add_column(u'catering_catering', 'cardinalita',
                      self.gf('django.db.models.fields.IntegerField')(default=0, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Catering.cardinalita'
        db.delete_column(u'catering_catering', 'cardinalita')


    models = {
        u'catering.categoriacatering': {
            'Meta': {'object_name': 'CategoriaCatering'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'principale': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'catering.catering': {
            'Meta': {'ordering': "['-cardinalita']", 'object_name': 'Catering'},
            'cardinalita': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.Cliente']", 'null': 'True', 'blank': 'True'}),
            'evidenza': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '125'}),
            'testo': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catering.CategoriaCatering']"}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '101'})
        },
        u'catering.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'cognome': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'catering.immaginecatering': {
            'Meta': {'ordering': "['-cardinalita']", 'object_name': 'ImmagineCatering'},
            'cardinalita': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'catering': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'galleria'", 'to': u"orm['catering.Catering']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immagine': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'})
        },
        u'catering.messaggio': {
            'Meta': {'object_name': 'Messaggio'},
            'cognome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'testo': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['catering']