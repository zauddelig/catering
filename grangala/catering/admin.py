from django.contrib import admin
from .models import Cliente, CategoriaCatering,Catering,ImmagineCatering,Messaggio
from sorl.thumbnail.admin import AdminImageMixin

class ImmagineCateringTabular(AdminImageMixin,admin.TabularInline):
    model = ImmagineCatering
    pass

class CateringAdmin(AdminImageMixin,admin.ModelAdmin):
    prepopulated_fields = {"slug": ("titolo",)}
    inlines = (ImmagineCateringTabular,)

admin.site.register(Cliente)
admin.site.register(CategoriaCatering)
admin.site.register(Catering, CateringAdmin)
admin.site.register(ImmagineCatering)
admin.site.register(Messaggio)