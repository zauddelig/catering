from django.views.generic.base import TemplateView
from django.core.paginator import Paginator
from django.views.generic.list import ListView
from django.http.response import HttpResponseRedirectBase, HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import Catering
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView
from .forms import MessaggioForm
class PaginaPrincipale(CreateView):
    template_name="catering/home.html"
    form_class = MessaggioForm
    success_url = '/'
    pass

class SingleCateringView(DetailView):
    template_name="partials/comments.html"
    pk_url_kwarg ='id'
    model = Catering
    pass

class CateringView(ListView):
    template_folder = 'catering/'
    template_filename = 'catering_list.html'
    html_prefix = 'html_'
    ajax_prefix = 'ajax_'
    model = Catering      # shorthand for setting queryset = models.Car.objects.all()
    template_name = template_folder + template_filename # optional (the default is app_name/modelNameInLowerCase_list.html; which will look into your templates folder for that path and file)
    context_object_name = "lista_catering"    #default is object_list as well as model's_verbose_name_list and/or model's_verbose_name_plural_list, if defined in the model's inner Meta class
    paginate_by = 3 
    def get_context_data(self, **kwargs):
        context = super(CateringView, self).get_context_data(**kwargs)
        context['url']=reverse('catering', kwargs={"tipo":self.kwargs.get('tipo')})
        return context
    def get_template_names(self):
        lst = []
        if self.request.is_ajax():
            lst.append(self.template_folder + self.ajax_prefix + self.template_filename)
            pass
        else:
            lst.append(self.template_folder + self.html_prefix + self.template_filename)
            pass
        return lst
    def get_queryset(self):
        tipo = self.kwargs.get('tipo').replace('_', ' ')
        return self.model.objects.filter(tipo__nome__iexact=tipo)
    pass