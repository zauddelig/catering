from django.conf.urls import patterns, include, url
from .views import (PaginaPrincipale, CateringView, SingleCateringView)

urlpatterns = patterns('',

    url(r'^catering/(?P<tipo>\w+)$', CateringView.as_view(), name="catering" ),
    url(r'^$', PaginaPrincipale.as_view(), name="home"),
    url(r'^catering/commenti/(?P<id>\d+)$', SingleCateringView.as_view(), name="single_catering" ),
)
