$(function(){
  	$.ajaxSetup({ cache: true });
  	$.getScript('//connect.facebook.net/it_IT/all.js', function(){
		FB.init({
 			appId: '613137332075449',
		});
		
});

$(
	function(){
		
		var disqus_url= 'http://scardacicatering.it',
		disqus_identifier ='',
		comment ={
			base_url:'http://scardacicatering.it',
			disqus_shortname : 'scardacicatering',
			count_src: function(){
				return '//' + comment.disqus_shortname + '.disqus.com/count.js'
			},
			count:function(){
				var s = document.createElement('script'); s.async = true;
			        s.type = 'text/javascript';
			        s.src = comment.count_src();
			        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
				return comment
				},
			embed_src:function(){
				return '//' + comment.disqus_shortname + '.disqus.com/embed.js'
				},
			embed:function(){
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			            dsq.src = comment.embed_src();
			            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				return comment
			},
			reset:function(options){
				DISQUS.reset({
				  reload: true,
				  config: function () {  
				    this.page.identifier = options.identifier;  
				   	this.page.url = comment.base_url ;
					}
				})
				return comment
			},
			disqus:function(){
				$('a.commenta').on('click', function(ev){
					ev.preventDefault()
					$('#modal').modal();
					var options ={
						identifier: $(this).attr('data-disqus-identifier'),
						url:$(this).attr('href')
						};
					try{
						comment.reset(options);
					}catch(err){
						disqus_identifier =options.identifier;
						disqus_url =comment.base_url;
						comment.embed();
					}
				})
			}
		},
	    mobile_collapse ={
			mouse:true,
			init : function(){
				var menu = $('#menu-collapse');
				menu.on('shown.bs.collapse',function(ev){
					mobile_collapse.mouse = false;
					menu.find('a').on('click',function(){
						if(!$(this).hasClass('dropdown-toggle'))
						menu.collapse('hide');
					});
				});
			}
			
		},
		scimip ={
			options:{
				startSlideshow: true,
				fullScreen: true,
				toggleControlsOnReturn: false,
			},
			gallery: function($e){
				blueimp.Gallery($e.children('a'), scimip.options)
			},
			init: function(){
				$('.links').on('click',
				function(ev){
					ev.preventDefault();
					scimip.gallery($(this));
				});
				$('.links-ref').on('click',
				function(ev){
					console.log('ev')
					ev.preventDefault();
					scimip.gallery($(this).closest('div').prev());
				});
			
			}
		}
		scimip.init();
		
		var open = [false, null],
		aside = [$('aside#contatti'), false];
		navs = $('nav .nav a.slider_trigger'),
		container = $('#content'),
		trigger_fb_feed = function(){
			$('.feed-trigger').bind('click', function(ev){
			ev.preventDefault()
			var data=$(this).find('i').data();
			FB.ui({
				method: 'feed',
				name: data['name'],
				link: data['link'],
				picture: data['picture'],
				caption: data['name'],
				description: data['description']
			});
		})
		},
		showSlide = function(){
			open[1].show("slide");
		},
		resetOpen = function(){
			if(open[1] != null){
				open[1].remove();
			}
			open = [false, null];
		},
		removeCurrentSlide = function(){
			if(open[1] != null){
				open[1].toggle('slide',complete=resetOpen);
			}
		},
		init_page_nav = function(){
			
		},
		paginator_init =function(){
			$('.pager a').on('click',function(ev){
				ev.preventDefault();
				href = $(this).attr('href');
				$.ajax({
						'url':href,
						'success':navCallBack
					});
			});
		},
		navCallBack =function(data, stat, x){
			var complete = function(){
				resetOpen();
				container.append(data);
				open=[href, $('#slide_container')];
				showSlide();
				paginator_init();
				scimip.init();
				comment.count().disqus();
				trigger_fb_feed();
			},
			href = open[0];
			if(open[1] != null){
				open[1].toggle('slide', complete = complete);
			}else{
				complete();
			};
			
		},
		init_catering_nav = function(){
			navs.on('click',function(ev){
				ev.preventDefault();
				
				var href = $(this).attr('href'),
				small = !(mobile_collapse.mouse&&$('.navbar-header').width()!=0),
				toggleContacts=function(){
					aside[0].toggle('slide', {direction:'right'});
					aside[1]=!aside[1];
				};	
				if (href=='#'){
					!aside[1]&&small&&removeCurrentSlide();
					toggleContacts()
				}else if(open[0] != href){
					small&&aside[1]&&toggleContacts();
					open[0] = href;
					$.ajax({
						'url':href,
						'success':navCallBack,
						'error':function(xhr,e,t){console.log(e);console.log(t);console.log(xhr)}
					});
					
				}else{
					removeCurrentSlide();
				}
			});
		},
		init_popover = function(){
			/*var nav =$('nav');
			nav.popover(
				{
					trigger:'manual',
					placement:'bottom',
					delay:{ show: 1000, hide: 3000 },
					content:'Usa il menu per navigare fra le categorie',
					
				}
				
			
			).popover('toggle').on('shown.bs.popover',function(){
				nav.popover('toggle');
				});*/
		},

		dropdown_menu ={
			dropdown : $('.dropdown'),
			mouseIn:function(ev){
				mobile_collapse.mouse && $(this).children().last().slideDown();
			},
			muoseOut:function(ev){
				mobile_collapse.mouse && $(this).children().last().slideUp();
			},
			init:function(){
				var klass=dropdown_menu;
				klass.dropdown.hover(klass.mouseIn, klass.muoseOut);
				klass.dropdown.children('a').on('click',function(){
					!mobile_collapse.mouse && $(this).next().toggle();
				})
			}
		},
		
		init = function(){
			
			aside[0].hide();
			init_catering_nav();
			init_page_nav();
			dropdown_menu.init();
			init_popover();
			mobile_collapse.init();
			trigger_fb_feed();
		};
		
		init();
		
	});
	}
);
