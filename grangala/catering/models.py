from django.db import models as m

from sorl.thumbnail.fields import ImageField
from newsletter.models import Newsletter, Subscription
from django.utils.timezone import now
from django.core.urlresolvers import reverse

CATERING = 'catering'
class Cliente(m.Model):
    nome = m.CharField(max_length = 50)
    cognome = m.CharField(max_length = 50)
    email = m.EmailField()
    def __unicode__(self):
        return self.nome + self.cognome
    def save(self, *args, **kwargs):
        try:
            newsletter = Newsletter.objects.get(slug='default')
        except:
            newsletter = Newsletter.objects.create(slug="default", title="default")
        self.sottoscrizione = Subscription.objects.create(
                    email = self.email,
                    subscribed = True,
                    subscribe_date = now,
                    name_field = self.name,
                    newsletter= newsletter,
                    )
        super(Cliente, self).save(*args,**kwargs)
    class Meta:
        verbose_name_plural ="Clienti"
        pass
    
class CategoriaCatering(m.Model):
    nome = m.CharField(max_length = 50, db_index=True)
    principale = m.BooleanField(default=False)
    def __unicode__(self):
        principale = self.principale and 'principale' or 'location'
        return self.nome+' - '+principale
    
    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        kwargs={'tipo':self.nome.replace(' ', '_')}
        return reverse('catering',kwargs=kwargs)
        pass
    
    class Meta:
        verbose_name="Categoria catering"
        verbose_name_plural ="Categorie di catering"
        pass
    pass

class Catering(m.Model):
    def getimage_url(self, nome):
        print nome
        nome =nome.rsplit('.',1)[1]
        return  '%s/%s/%s/evidenza.%s'%(CATERING, self.tipo.nome,self.titolo, nome)
    evidenza = ImageField(upload_to=getimage_url)
    testo = m.TextField()
    titolo = m.CharField(max_length = 101)
    slug = m.SlugField(max_length = 125)
    cliente = m.ForeignKey(Cliente, null= True, blank = True)
    tipo = m.ForeignKey(CategoriaCatering, verbose_name="categoria")
    cardinalita = m.IntegerField(blank=True, default=0)
    data = m.DateTimeField(auto_now_add = True)
    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('single_catering', kwargs={'id':self.pk})
    def __unicode__(self):
        return self.tipo.nome+' - '+self.titolo
    class Meta:
        ordering = ['-cardinalita']
        verbose_name_plural ="I catering"
        pass
    pass

class ImmagineCatering(m.Model):
    def getimage_url(self, nome):
        nome =nome.rsplit('.',1)[1]
        return  '%s/%s/%s.%s'%(CATERING,self.catering.tipo.nome, self.pk, nome)
    immagine = ImageField(upload_to=getimage_url)
    catering = m.ForeignKey(Catering, related_name="galleria")
    cardinalita = m.IntegerField(default=0)
    data = m.DateTimeField(auto_now_add = True)
    class Meta:
        verbose_name="Immagine galleria catering"
        verbose_name_plural ="Immagini della galleria del catering"
        ordering = ['-cardinalita']
        pass
    pass

from django.core.mail.backends import smtp
from django.core.mail.message import EmailMultiAlternatives
from grangala.lista_email.models import Utente, OrigineUtente
origine = OrigineUtente.objects.get(origine='contatti')
class Messaggio(m.Model):
    nome = m.CharField(max_length = 30)
    cognome = m.CharField(max_length = 30)
    email = m.EmailField()
    telefono = m.CharField(max_length = 30)
    testo = m.TextField()
    def __unicode__(self):
        return self.nome+' '+self.cognome+' - '+self.email+' - '+self.testo[:20]
    def save(self, *args, **kwargs):
        super(Messaggio,self).save(*args,**kwargs)
        try:
            indirizzo = 'info@scardacicatering.it'
            username = "scardacicatering@gmail.com"
            password = "Sc4rdaci"
            connection = smtp.EmailBackend('smtp.gmail.com',587, username, password, use_tls =True) 
            corpo = self.nome+' '+self.cognome+'\n'+self.email+' - '+ self.telefono +'\n'+self.testo
            EmailMultiAlternatives(
                'Nuova richiesta di contatto', corpo,
                from_email = username,
                to=(indirizzo,),
                headers={
                         'Reply-To':self.email,
                         },
                connection = connection,
            ).send()
        except:
            pass
        try:
            Utente.objects.create(nome=self.nome+' '+self.cognome, email=self.email,origine=origine)
        except:
            pass
        