from django.core.management.base import BaseCommand
from grangala.lista_email.importatore import importa_utente
from grangala.lista_email.models import Utente, CodicePromozionale, Promozione
import poplib
from django.db import transaction
import base64
class Command(BaseCommand):
    #user = 'email@scardacicatering.it'
    user = 'info@scardacicatering.zapto.org'
    password ='scardaci'
    def handle(self, *args, **kwargs):
        self.apri_pop()
        num_mails = len(self.mailbox.list()[1])
        for i in range(num_mails):
            self.stdout.write('%s su %s'% (i+1,num_mails))
            for j in range(len(self.mailbox.retr(i+1))):
                txt= ''
                
                if type(self.mailbox.retr(i+1)[j]) is list:
                    b64 = False
                    inizio = 0
                    fine =0
                    for n in range(len(self.mailbox.retr(i+1)[j])):
                        if "Content-Transfer-Encoding: base64" == self.mailbox.retr(i+1)[j][n]:
                            b64= True
                            inizio = n+2
                        if b64 == True:
                            if "-boundary" in self.mailbox.retr(i+1)[j][n]:
                                fine= n-1
                    if inizio != fine:
                        txt = base64.b64decode(''.join(self.mailbox.retr(i+1)[j][inizio:fine]))
                    else:
                        txt ='\n'.join(self.mailbox.retr(i+1)[j])
                else:
                    txt = self.mailbox.retr(i+1)[j]
                if txt == '':
                    continue
                try:
                    self.apri_mail(txt)
                except Exception as e:
                    self.stdout.write(e.message)
                    continue
            pass
        self.stdout.write('completato processo lettura')
        #for i in range(num_mails):
            #self.mailbox.dele(i+1)
        #    pass
        self.chiudi_pop()
    
    def apri_pop(self):
        self.mailbox = poplib.POP3('localhost')
        self.mailbox.user(self.user)
        self.mailbox.pass_(self.password)
        
    def chiudi_pop(self): 
        self.mailbox.quit()
        
    def apri_mail(self, txt):
        try:
            
            nome, citta, email, data = importa_utente(txt,self)
        except Exception as e:
            self.stdout.write('email non letta, eccezione %s' %e.message)
        else:
            self.stdout.write('email letta')
            utente, neo = Utente.objects.get_or_create(nome = nome, citta= citta,
                 email = email, data_nozze=data
                )
	    if neo:
	      CodicePromozionale.objects.create(promozione=Promozione.objects.all[0], utente=utente)
        pass
        
        
    pass
