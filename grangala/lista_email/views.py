from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from .forms import Rinuncia
from .models import Utente
from django.core.urlresolvers import reverse_lazy

class RinunciaIscrizione(FormView):
    template_name = 'lista_email/rinuncia.html'
    form_class = Rinuncia
    success_url = reverse_lazy('rinuncia_utente_lista_riuscita')
    
    def _moltiplicazione(self,n):
        #insieme di numeri primi
        return (n*7+47)*103+587
    def divisione(self):
        user = int(self.user)-587
        if user%103!=0:
            return False
        user/=103
        user -= 47
        if user%7!=0:
            return False
        self.user = user/7
        return True
    def form_valid(self, form):
        self.user = self.kwargs.get('user')
        print form.cleaned_data['rinuncia']
        print form.cleaned_data['rinuncia'] and self.divisione()
        if form.cleaned_data['rinuncia'] and self.divisione():
            try:
                utente = Utente.objects.get(pk=self.user)
                if not utente.disponibile:
                    self.success_url = reverse_lazy('rinuncia_utente_non_riuscita')
                else:
                    utente.rimuovi_utente()
            except:
                self.success_url = reverse_lazy('rinuncia_utente_non_riuscita')
        elif not form.cleaned_data['rinuncia']:
            self.success_url = reverse_lazy('rinuncia_utente_non_effettuata')
        else:
            self.success_url = reverse_lazy('rinuncia_utente_non_riuscita')
        return super(RinunciaIscrizione, self).form_valid(form)

class RinunciaRiuscita(TemplateView):
    template_name = 'lista_email/rinuncia_riuscita.html'

class RinunciaNonRiuscita(TemplateView):
    template_name = 'lista_email/rinuncia_non_riuscita.html'

class RinunciaNonEffettuata(TemplateView):
    template_name = 'lista_email/rinuncia_non_effettuata.html'