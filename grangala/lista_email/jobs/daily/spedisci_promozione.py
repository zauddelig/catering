import logging
from newsletter.jobs.hourly import submit
from django_extensions.jobs.daily import daily_cleanup
import smtplib
from django.core.urlresolvers import reverse
logger = logging.getLogger(__name__)
from django_extensions.management.jobs import DailyJob
from grangala.lista_email.models import CodicePromozionale as CP
from newsletter.models import Submission
from django.template import loader
from django.template.context import Context
from django.core.mail.message import EmailMessage, EmailMultiAlternatives
from django.core.mail.backends import smtp
class Job(DailyJob):
    help = "Email promozionali per utenti sposa magazine"
    template = loader.get_template('email/message.html')
    txt_template = loader.get_template('email/message.txt')
    indirizzo = 'info@scardacicatering.it'
    username = "scardacicatering@gmail.com"
    password = "Sc4rdaci"
    def execute(self):
        def calcola_codice_utente(n):
            return (n+47)*103+587
        print 'inizio'
        connection = smtp.EmailBackend('smtp.gmail.com',587, self.username, self.password, use_tls =True) 
        # executing empty sample job
        codici = CP.objects.filter(inviato=False, utente__disponibile = True).select_related('utente','promozione')
        print codici
        for codice in codici:
            rinuncia ='http://scardacicatering.it/newsletter/rimuovi_sottoscrizione/'+ str(calcola_codice_utente(codice.utente.pk))
            contesto=Context({
                      'incipit':codice.incipit,                                                                                                                                                                                                                                                           
                      'nome': codice.utente.origine.messaggio.testo,
                      'codice':codice.codice_sconto,
                      'validita':codice.validita(),
                      'percentuale_sconto':codice.promozione.percentuale_sconto,
                      'codice_utente' : calcola_codice_utente(codice.utente.pk),
                      'mailto':self.username,
                      'rinuncia':rinuncia,
                      })
            corpo = self.txt_template.render(contesto)
            
            message = EmailMultiAlternatives(
                    codice.promozione.titolo, corpo,
                    from_email = self.username,
                    to=(codice.utente.email,),
                    headers={
                             'From':'"Scardaci Catering" <%s>'%self.indirizzo,
                             'Reply-To':self.indirizzo,
                             'Precedence': 'bulk',
                             'List-Unsubscribe':rinuncia,
                             },
                    connection = connection,
                )
            message.attach_alternative(
                        self.template.render(contesto),
                        "text/html"
                    )
            print 'messaggio scritto invio messaggio'
            try:
                message.send()
            except Exception as e:
                logger.error("invio all'email %s fallito" % codice.utente.email)
                print e.msg
                print "invio all'email %s fallito"
            else:
                codice.inviato = True
                codice.save()
            continue
        connection.close()
        pass
