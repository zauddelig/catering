from django.contrib import admin, messages
from django.utils.translation import ugettext
from django.http import HttpResponse, HttpResponseRedirect
from .models import Utente,CodicePromozionale, Promozione, MessaggioPersonalizzato, OrigineUtente
from django.contrib.admin.options import ModelAdmin
from django.conf.urls import patterns,url
from grangala.lista_email.jobs.daily import spedisci_promozione
from django.shortcuts import render
#Todo ricerca in Codice Promozionale

class PromozioneAdmin(ModelAdmin):
    def get_urls(self):
        urls = super(PromozioneAdmin, self).get_urls()
        my_urls = patterns('',
                         url(r'^(?P<pk>\d+)/preview/$', self.admin_site.admin_view(self.preview)),
                         url(r'^invia/$', self.admin_site.admin_view(self.invia))
                         )
        return my_urls+urls
        pass
    def preview(self, request, pk):
        promozione = self.model.objects.get(pk=pk)
        return render(request, 'email/message.html', {
                'incipit': promozione.incipit,
                'nome': 'Utente Prova',
                'codice':'00000',
                'validita':promozione.validita,
                'percentuale_sconto':promozione.percentuale_sconto,
                'codice_utente' : '00000',
                'mailto':'info@scardacicatering.it',
                'rinuncia':'rinuncia',
                })
        pass
    def invia(self, request):
        messages.info(request, ugettext("inviato."))
        return HttpResponseRedirect('../')
        pass
    pass

class CodicePromozionaleAdmin(ModelAdmin):
    job = spedisci_promozione.Job()
    job.execute()
    search_fields = ['codice_sconto',]
    readonly_fields = ['codice_sconto', 'validita']
    pass
    
admin.site.register(CodicePromozionale, CodicePromozionaleAdmin)
admin.site.register(Utente)
admin.site.register(Promozione,PromozioneAdmin)
admin.site.register(MessaggioPersonalizzato)
admin.site.register(OrigineUtente)