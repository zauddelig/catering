from grangala.lista_email.models import CodicePromozionale
from django.core.cache import cache
from django import template

register = template.Library()

@register.filter
def codice_promozionale(utente, promozione):
    c = cache.get(str(utente)+str(promozione), None)
    if c is not None:
        return c.codice
    else:
        obj = CodicePromozionale.objects.get(promozione= promozione, utente=utente)
        cache.set(str(utente)+str(promozione), obj)
        return  obj.codice
  
@register.filter  
def validita_codice(utente, promozione):
    c = cache.get(str(utente)+str(promozione), None)
    if c is not None:
        cache.delete(str(utente)+str(promozione))
        return c.validita
    else:
        obj = CodicePromozionale.objects.get(promozione= promozione, utente=utente)
        return  obj.validita
    pass

@register.filter 
def get_variabile(txt):
    return 'info@scardacicatering.it'