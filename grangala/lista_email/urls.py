from django.conf.urls import patterns, include, url
from grangala.lista_email.views import RinunciaIscrizione, RinunciaRiuscita,RinunciaNonEffettuata,RinunciaNonRiuscita
urlpatterns = patterns('',
    url(r'rimuovi_sottoscrizione/(?P<user>\d+)', RinunciaIscrizione.as_view(), name="rinuncia_iscrizione"),
    url(r'rinuncia_riuscita',RinunciaRiuscita.as_view(), name="rinuncia_utente_lista_riuscita" ),
    url(r'rinuncia_non_riuscita', RinunciaNonRiuscita.as_view(), name="rinuncia_utente_non_riuscita"),
    url(r'rinuncia_non_effettuata',RinunciaNonEffettuata.as_view(), name="rinuncia_utente_non_effettuata"),
)
#!TODO: qualcosa di sbagliato, ma non riesco a capire cosa..