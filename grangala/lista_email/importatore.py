from django.utils.html import strip_tags
import re
from django.core.validators import validate_email
import time
import base64

class Utente(object):
    command = None
    string = ''
    regex_nome = ''
    nome_tovato= False
    ciclo_nome = True
    ciclo_email = True
    nome=None;citta=None;email=None;giorno=None;
    b64 = False; b64string= ''; b64complete = False
    def scrivi(self, mess):
        if self.command:
            self.command.stdout.write(mess)
    def __init__(self, html,  command= None):
        self.regex_nome = re.compile("^(\w+ \w+)$",re.MULTILINE)
        self.command = command
        self.scrivi('inizio')
        self.string = strip_tags(html)
        for line in self.string.splitlines():
            if self.giorno is not None:
                break
            line = str(line).strip(None)
            self.componi_email(line)
        if self.nome is None:
            raise Exception
        
    def componi_email(self,line):
        if not self.nome_tovato:
            r = self.regex_nome.search(line)
            if r and line != "Sposi Magazine":
                self.scrivi(line)
                self.nome_tovato= True
                
                self.nome = r.groups(1)[0]
                
        elif self.ciclo_nome:
            #solitamente la citta' sta qua
            if "=" in line:
                pass
            else:
                self.citta = line.rsplit(' ', 1)[1]
                self.ciclo_nome = False
                
                self.scrivi(self.citta)
        elif self.ciclo_email:
            #solitamente l'ultima stringa e' un'email
            
            
            try:
                validate_email(line)
                self.ciclo_email = False
            except Exception as e:
                self.scrivi(line)
                '''
                log = open('log/importer.log','w')
                log.write(e)
                
                log.close()
                '''
            else:
                self.email = line
                self.scrivi(self.email)
            
        else: 
            #manca solo la data
            self.scrivi('cerco data')
            try:
                self.giorno = time.strftime('%Y-%m-%d',time.strptime(line, '%d/%m/20%y'))
            except:
                pass
            else:
                self.scrivi(str(self.giorno))
        pass
    
def importa_utente(html,  command= None):
    utente = Utente(html, command)
    return utente.nome, utente.citta, utente.email, utente.giorno
