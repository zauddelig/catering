from newsletter.models import Newsletter, Subscription, Message
from django.db import models as m
from django.utils.timezone import now
from datetime import datetime, date, timedelta
import random

def calcola_codice_sconto():
        return random.randint(10000, 99999)
class OrigineUtente(m.Model):
    origine = m.TextField()
    def __unicode__(self):
        return self.origine
    pass
class MessaggioPersonalizzato(m.Model):
   origine= m.OneToOneField(OrigineUtente, related_name="messaggio")
   testo=m.TextField()
   def __unicode__(self):
        return str(self.origine)
   pass
class Utente(m.Model):
    nome = m.CharField(max_length=100)
    citta = m.CharField(max_length=40, null=True)
    email = m.EmailField(unique = True)
    data_nozze = m.DateField(null=True)
    disponibile = m.BooleanField(default=True, editable=True)
    sottoscrizione = m.OneToOneField(Subscription, blank=True, null=True, related_name="utente")
    origine=m.ForeignKey(OrigineUtente, default=OrigineUtente.objects.get(origine='sposimagazine'))
    def rimuovi_utente(self):
        self.disponibile = False
        self.save()
    def save(self, *args, **kwargs):
        
        try:
            newsletter = Newsletter.objects.get(slug='default')
        except:
            newsletter = Newsletter.objects.create(slug="default", title="default")
        if self.sottoscrizione is None:
            try:
                self.sottoscrizione = Subscription.objects.create(
                        email_field = self.email,
                        subscribed = True,
                        subscribe_date = now,
                        name_field = self.nome,
                        newsletter= newsletter,
                        )
            except:
                print 'non sottoscritto'
        if not self.disponibile:
            try:
                self.sottoscrizione.subscribed=False
                self.sottoscrizione.unsubscribed=True
                self.sottoscrizione.save()
            except:
                pass
        if not CodicePromozionale.objects.filter(utente=self):
            for promozione in Promozione.objects.all().filter(attiva=True):
                if promozione.validita is not None and promozione.validita<date.today():
                    promozione.attiva = False
                    promozione.save()
                    continue
                try:
                    CodicePromozionale.objects.create(promozione=promozione, utente=self)
                except:
                    continue
                pass
            pass
        super(Utente, self).save(*args,**kwargs)
        pass
    def __unicode__(self):
        string = self.nome
        string += ' a '+self.citta
        string += ' il '+self.data_nozze.strftime('%d/%m/%Y')
        return string
    class Meta:
        verbose_name = 'utente Sposi Magazine'
        verbose_name_plural = 'utenti Sposi Magazine'

class Promozione(m.Model):
    nome = m.CharField(max_length = 60)
    titolo = m.CharField(max_length = 60)
    percentuale_sconto = m.IntegerField()
    validita = m.DateField(blank=True, null=True)
    validita_num_giorni = m.IntegerField(blank=True, null=True)
    incipit = m.CharField(max_length = 200, default="In seguito alla tua iscrizione a Sposi Magazine")
    messaggio = m.OneToOneField(Message, related_name= 'promozione', null = True, blank=True)
    attiva = m.BooleanField(default=True)
    sconto = m.TextField(default='5%')
    def __unicode__(self):
        if self.validita is not None:
            validita = (self.validita>date.today() and self.attiva) and 'attiva' or 'non attiva'
        else:
            validita = self.attiva and 'attiva' or 'non attiva'
        return self.nome+' - '+validita
    def save(self, *args, **kwargs):
        super(Promozione, self).save(*args, **kwargs)
        for utente in Utente.objects.all().filter(disponibile=True):
            try:
                utente.promozioni.get(promozione=self)
            except:
                utente.promozioni.create(promozione=self)
    class Meta:
        verbose_name_plural = 'promozione'
    pass


class CodicePromozionale(m.Model):
    promozione = m.ForeignKey(Promozione, related_name = "codici")
    utente = m.ForeignKey(Utente, related_name="promozioni")
    creato_il = m.DateTimeField(auto_now_add=True)
    codice_sconto = m.CharField(max_length = 10, blank = True, editable=False, default=calcola_codice_sconto)
    inviato = m.BooleanField(blank=True,default=False)
    validita_field = m.DateField(blank=True, null=True)
    usato = m.BooleanField(default=False)
    sconto = m.TextField(default='5%')
    class Meta:
        unique_together = (('promozione', 'utente'),)
        verbose_name_plural = 'codici promozionali ed email'
    def  __unicode__(self):
        disponibilita = self.usato and 'non disponibile' or 'disponibile'
        return 'codice: %s - cliente: %s - %s'%(self.codice_sconto, self.utente.nome, disponibilita)
    def validita(self):
        if self.validita_field is not None:
            return self.validita_field
        else:
            try:
                return timedelta(self.promozione.validita_num_giorni) + self.creato_il
            except:
                return '20/04/2014'
    def incipit(self):
        return self.promozione.incipit
    def save(self, *args, **kwargs):
        if self.promozione.validita is not None:
            validita_field = self.promozione.validita
        self.sconto = self.promozione.sconto
        super(CodicePromozionale, self).save(*args, **kwargs)