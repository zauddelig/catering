# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'MessaggioPersonalizzato.origine'
        db.alter_column(u'lista_email_messaggiopersonalizzato', 'origine_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['lista_email.OrigineUtente'], unique=True))
        # Adding unique constraint on 'MessaggioPersonalizzato', fields ['origine']
        db.create_unique(u'lista_email_messaggiopersonalizzato', ['origine_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'MessaggioPersonalizzato', fields ['origine']
        db.delete_unique(u'lista_email_messaggiopersonalizzato', ['origine_id'])


        # Changing field 'MessaggioPersonalizzato.origine'
        db.alter_column(u'lista_email_messaggiopersonalizzato', 'origine_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lista_email.OrigineUtente']))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'lista_email.codicepromozionale': {
            'Meta': {'unique_together': "(('promozione', 'utente'),)", 'object_name': 'CodicePromozionale'},
            'codice_sconto': ('django.db.models.fields.CharField', [], {'default': '19796', 'max_length': '10', 'blank': 'True'}),
            'creato_il': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inviato': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'promozione': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'codici'", 'to': u"orm['lista_email.Promozione']"}),
            'sconto': ('django.db.models.fields.TextField', [], {'default': "'5%'"}),
            'usato': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'utente': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'promozioni'", 'to': u"orm['lista_email.Utente']"}),
            'validita_field': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'lista_email.messaggiopersonalizzato': {
            'Meta': {'object_name': 'MessaggioPersonalizzato'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'origine': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['lista_email.OrigineUtente']", 'unique': 'True'}),
            'testo': ('django.db.models.fields.TextField', [], {})
        },
        u'lista_email.origineutente': {
            'Meta': {'object_name': 'OrigineUtente'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'origine': ('django.db.models.fields.TextField', [], {})
        },
        u'lista_email.promozione': {
            'Meta': {'object_name': 'Promozione'},
            'attiva': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incipit': ('django.db.models.fields.CharField', [], {'default': "'In seguito alla tua iscrizione a Sposi Magazine'", 'max_length': '200'}),
            'messaggio': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'promozione'", 'unique': 'True', 'null': 'True', 'to': u"orm['newsletter.Message']"}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'percentuale_sconto': ('django.db.models.fields.IntegerField', [], {}),
            'sconto': ('django.db.models.fields.TextField', [], {'default': "'5%'"}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'validita': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'validita_num_giorni': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'lista_email.utente': {
            'Meta': {'object_name': 'Utente'},
            'citta': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'data_nozze': ('django.db.models.fields.DateField', [], {}),
            'disponibile': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'origine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lista_email.OrigineUtente']"}),
            'sottoscrizione': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'utente'", 'unique': 'True', 'null': 'True', 'to': u"orm['newsletter.Subscription']"})
        },
        u'newsletter.message': {
            'Meta': {'unique_together': "(('slug', 'newsletter'),)", 'object_name': 'Message'},
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_modify': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'newsletter': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['newsletter.Newsletter']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'newsletter.newsletter': {
            'Meta': {'object_name': 'Newsletter'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'send_html': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'site': ('django.db.models.fields.related.ManyToManyField', [], {'default': '[1]', 'to': u"orm['sites.Site']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'})
        },
        u'newsletter.subscription': {
            'Meta': {'unique_together': "(('user', 'email_field', 'newsletter'),)", 'object_name': 'Subscription'},
            'activation_code': ('django.db.models.fields.CharField', [], {'default': "'67b0f668fd77c48e3f47ff9f4630ea457260bd6f'", 'max_length': '40'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email_field': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'max_length': '75', 'null': 'True', 'db_column': "'email'", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'name_field': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'db_column': "'name'", 'blank': 'True'}),
            'newsletter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['newsletter.Newsletter']"}),
            'subscribe_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'subscribed': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'unsubscribe_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'unsubscribed': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['lista_email']