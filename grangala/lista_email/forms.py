from django import forms as f

class Rinuncia(f.Form):
    rinuncia = f.BooleanField(required=False)